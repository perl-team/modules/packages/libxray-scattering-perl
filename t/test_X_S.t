#!/usr/bin/perl -w                ## -*- cperl -*-

use Xray::Scattering;

my $d = 3;
my $symb = Xray::Scattering->get_valence('F', -1);

BEGIN { $| = 1; print "1..5$/"; }
END {print "not ok 1$/" unless $loaded;}
use Xray::Absorption;
$loaded = 1;
$i = 0;
print "ok ", ++$i, $/;

if ($symb eq q{F1-}) {
  print "ok ", ++$i, $/
} else {
  print "not ok ", ++$i, $/
};

#printf "Cromer-Mann:      %s  %s  %s\n",
#  $symb, $d, Xray::Scattering->get_f($symb, $d);
#print join(" ", Xray::Scattering->get_coefficients($symb)), $/;

if (abs(7.619 - Xray::Scattering->get_f($symb, $d)) < 0.01) {
  print "ok ", ++$i, $/
} else {
  print "not ok ", ++$i, $/
};


Xray::Scattering -> load('waaskirf');
#printf "Waasmaier-Kirfel: %s  %s  %s\n",
#  $symb, $d, Xray::Scattering->get_f($symb, $d);
#print join(" ", Xray::Scattering->get_coefficients($symb)), $/;

if (abs(7.620 - Xray::Scattering->get_f($symb, $d)) < 0.01) {
  print "ok ", ++$i, $/
} else {
  print "not ok ", ++$i, $/
};



Xray::Scattering -> load('none');
#printf "None:             %s  %s  %s\n",
#  $symb, $d, Xray::Scattering->get_f($symb, $d);
#print join(" ", Xray::Scattering->get_coefficients($symb)), $/;

if (Xray::Scattering->get_f($symb, $d) == 0) {
  print "ok ", ++$i, $/
} else {
  print "not ok ", ++$i, $/
};
